import React from "react";
import PropTypes from "prop-types";

const Navigation = (props) => {
	const {
		activeSection,
		lastSectionIndex,
		onSectionChange,
	} = props;

	return (
		<div className="scroll-nav">
			<span
				className={activeSection > 0 ? "active" : "inactive"}
				onClick={() => onSectionChange(-1)}
			>
        Previous
			</span>
			<span
				className={activeSection < lastSectionIndex ? "active" : "inactive"}
				onClick={() => onSectionChange(1)}
			>
        Next
			</span>
		</div>
	);
};

Navigation.propTypes = {
	activeSection: PropTypes.number.isRequired,
	lastSectionIndex: PropTypes.number.isRequired,
	onSectionChange: PropTypes.func.isRequired,
};

export default Navigation;
