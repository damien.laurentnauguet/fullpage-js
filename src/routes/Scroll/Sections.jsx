import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import useWindowSize from "../../hooks/useWindowSize";

const Sections = ({
	isWithOverflow, activeSection, sections, onSectionChanged,
}) => {
	// Use custom hook
	const [width] = useWindowSize();

	// Declare a local state to store current activeSection
	const [currentActiveSection, setCurrentActiveSection] = useState(activeSection);

	useEffect(() => {
		for (const [index, section] of Array.from(document.getElementsByClassName("section")).entries()) {
			section.style.zIndex = index;
			section.style.left = `${width * index}px`;
			section.style.backgroundColor = sections[index].backgroundColor;
		}
	}, [width, sections]);

	useEffect(() => {
		// Assess direction
		const direction = activeSection - currentActiveSection;

		// On initialisation, do nothing
		if (direction === 0)
			return;

		for (const [index, section] of Array.from(document.getElementsByClassName("section")).entries()) {
			if (isWithOverflow && index < activeSection)
				section.style.left = "0px";
			else
				section.style.left = `${width * (index - activeSection)}px`;

			setCurrentActiveSection(activeSection);
			onSectionChanged();
		}
	}, [activeSection]);

	return (
		<div className="sections">
			{sections.map((section) => (
				<div
					className="section"
					key={section.id}
				>
					{section.title}
				</div>
			))}
		</div>
	);
};

Sections.propTypes = {
	isWithOverflow: PropTypes.bool.isRequired,
	activeSection: PropTypes.number.isRequired,
	sections: PropTypes.array.isRequired,
	onSectionChanged: PropTypes.func.isRequired,
};

export default Sections;
