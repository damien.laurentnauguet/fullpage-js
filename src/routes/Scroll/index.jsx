import React, { useState, useEffect } from "react";

import Sections from "./Sections";
import Navigation from "./Navigation";

import "../../styles/scroll.scss";

export default () => {
	// Declare a local state to assess whether navigation is blocked
	const [isNavigationBlocked, setIsNavigationBlocked] = useState(false);

	// Declare a local state to assess whether page is loading
	const [isLoading, setIsLoading] = useState(true);

	// Declare a local state for all sections
	const [sections, setSections] = useState([]);

	// Declare another local state for active section
	const [activeSection, setActiveSection] = useState(0);

	// Mock a fetch from database
	useEffect(() => {
		setTimeout(() => {
			setIsLoading(false);
			setSections([{
				id: "asd4qwe6454",
				title: "Section A",
				backgroundColor: "#4285F4",
			}, {
				id: "as456dqwasd",
				title: "Section B",
				backgroundColor: "#DB4437",
			}, {
				id: "as45d4qwe4as",
				title: "Section C",
				backgroundColor: "#F4B400",
			}, {
				id: "as65f4da64ew",
				title: "Section D",
				backgroundColor: "#0F9D58",
			}]);
		}, 1500);
	}, []);

	// Declare a handler to manager section change
	const handleSectionChange = (direction) => {
		if (isNavigationBlocked || activeSection + direction < 0 || activeSection + direction >= sections.length)
			return;
		setActiveSection(activeSection + direction);
		setIsNavigationBlocked(true);
	};

	const handleSectionChanged = () => {
		setIsNavigationBlocked(false);
	};

	return (
		<div className="scroll">
			{isLoading
				? <div className="loading">Loading...</div>
				: (
					<React.Fragment>
						<Sections
							isWithOverflow
							activeSection={activeSection}
							sections={sections}
							onSectionChanged={handleSectionChanged}
						/>
						<Navigation
							activeSection={activeSection}
							lastSectionIndex={sections.length - 1}
							onSectionChange={(direction) => handleSectionChange(direction)}
						/>
					</React.Fragment>
				)}
		</div>
	);
};
