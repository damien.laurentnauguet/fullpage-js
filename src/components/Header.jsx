import React from "react";
import { Link } from "react-router-dom";

export default () => (
	<header>
		<Link to="/">HOME</Link>
		<Link to="/scroll">SCROLL</Link>
	</header>
);
