import React, { useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./components/Header";

import Home from "./routes/Home";
import Scroll from "./routes/Scroll";

const Application = () => {
	useEffect(() => {
		console.log("Application rendered.");
	}, []);

	return (
		<main className="App">
			<BrowserRouter>
				<Header />
				<Switch>
					<Route
						exact
						path="/"
					>
						<Home />
					</Route>
					<Route
						component={Scroll}
						path="/scroll"
					>
						<Scroll />
					</Route>
				</Switch>
			</BrowserRouter>
		</main>
	);
};

export default Application;
